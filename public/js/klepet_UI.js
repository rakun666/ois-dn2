function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function divElementSlika(smesko) {
  switch (smesko) {
    case(';)'):
      return ('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
    case(':)'):
      return ('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
    case('(y)'):
      return ('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
    case(':*'):
      return ('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
    case(':('):
      return ('<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');
    default:
      break;
  };
}

String.prototype.replaceAt=function(index, character) {
  return this.substr(0, index) + character + this.substr(index+character.length);
};

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    
    var zvezde = '';
    for (var i = 0; i < swearArray.length; i++) {
      for (var j = 0; j < sporocilo.length; j++) {
        if (sporocilo.substring(j + swearArray[i].length, j).toLowerCase() == swearArray[i]) {
          for (var k = 0; k < swearArray[i].length; k++) {
            zvezde += '*';
          }
          sporocilo = sporocilo.substring(j, 0) + zvezde + sporocilo.substring(sporocilo.length, j + zvezde.length);
          zvezde = '';
        }
      }
    }

    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    
    
    var konkatenacija = '<div style="font-weight: bold">';
    var start = 0;
    for (var i = 0; i < sporocilo.length; i++) {
      if (sporocilo.charAt(i) == ';' && sporocilo.charAt(i + 1) == ')') {
        konkatenacija += sporocilo.substring(start, i);
        konkatenacija += divElementSlika(';)');
        i++;
        start = i + 1;
      }
      if (sporocilo.charAt(i) == ':' && sporocilo.charAt(i + 1) == ')') {
        konkatenacija += sporocilo.substring(start, i);
        konkatenacija += divElementSlika(':)');
        i++;
        start = i + 1;
      }
      if (sporocilo.charAt(i) == ':' && sporocilo.charAt(i + 1) == '*') {
        konkatenacija += sporocilo.substring(start, i);
        konkatenacija += divElementSlika(':*');
        i++;
        start = i + 1;
      }
      if (sporocilo.charAt(i) == ':' && sporocilo.charAt(i + 1) == '(') {
        konkatenacija += sporocilo.substring(start, i);
        konkatenacija += divElementSlika(':(');
        i++;
        start = i + 1;
      }
      if (sporocilo.charAt(i) == '(' && sporocilo.charAt(i + 1) == 'y' && sporocilo.charAt(i + 2) == ')') {
        konkatenacija += sporocilo.substring(start, i);
        konkatenacija += divElementSlika('(y)');
        i += 2;
        start = i + 1;
      }
    }
    konkatenacija += sporocilo.substring(start, sporocilo.length);
    konkatenacija += '</div>';
    $('#sporocila').append($(konkatenacija));
    
    //$('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();
var trenutni_kanal_client = '';

var swearArray = ['anal','anus','arse','ass','ballsack','balls','bastard','bitch','biatch','bloody','blowjob','blow job','bollock','bollok','boner','boob','bugger','bum','butt','buttplug','clitoris','cock','coon','crap','cunt','damn','dick','dildo','dyke','fag','feck','fellate','fellatio','felching','fuck','f u c k','fudgepacker','fudge packer','flange','Goddamn','God damn','hell','homo','jerk','jizz','knobend','knob end','labia','lmao','lmfao','muff','nigger','nigga','omg','penis','piss','poop','prick','pube','pussy','queer','scrotum','sex','shit','s hit','sh1t','slut','smegma','spunk','tit','tosser','turd','twat','vagina','wank','whore','wtf'];

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var vzdevek_temp = '';
  var kanal_temp = '';


  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    vzdevek_temp = rezultat.vzdevek;
    $('#kanal').text(vzdevek_temp + ' @ ' + kanal_temp);
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    socket.emit('uporabniki');
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {

    kanal_temp = rezultat.kanal;
    $('#kanal').text(vzdevek_temp + ' @ ' + kanal_temp);
    trenutni_kanal_client = rezultat.kanal;

    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    socket.emit('uporabniki');
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    socket.emit('uporabniki');
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
      for(var i in uporabniki) {
        if (uporabniki[i][0] == trenutni_kanal_client) {
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i][1]));
        }
      }
  });
  
  setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);
  
  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});