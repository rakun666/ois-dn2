var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

var protectedKanali = [];
var protectedGesla = [];
var protectedVzdevki = [];

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      if (besede[1].charAt(0) == '"' && besede[1].charAt(besede[1].length - 1) == '"' && besede[2].charAt(0) == '"' && besede[2].charAt(besede[2].length - 1) == '"') {
        var i = protectedKanali.indexOf(besede[1].substring(besede[1].length - 1, 1))
        if (i > -1) {
          if (protectedGesla[i] == besede[2].substring(besede[2].length - 1, 1)) {
            this.spremeniKanal(besede[1].substring(besede[1].length - 1, 1));
            break;
          } else {
            sporocilo = 'Pridruzitev v kanal ' + kanal + ' ni bilo uspeno, ker je geslo napacno!';
            break;
          }
        } else {
          this.spremeniKanal(besede[1].substring(besede[1].length - 1, 1));
          protectedKanali.push(besede[1].substring(besede[1].length - 1, 1))
          protectedGesla.push(besede[2].substring(besede[2].length - 1, 1))
          break;
        }
      }
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      protectedVzdevki.push(vzdevek);
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      if (protectedVzdevki.indexOf(besede[1].substring(besede[1].length - 1, 1)) > -1) {
        this.socket.emit('sporocilo', besede[2]);
        break;
      } else {
        sporocilo = 'Sporocila ' + besede[2] + ' uporaniku z vzdevkom ' + besede[1] + ' ni bilo mogoce posredovati.';
        break;
      }
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};